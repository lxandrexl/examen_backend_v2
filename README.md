# Backend version 2

El examen de desarrollador - versión 2 con Framework Express y sintaxis en Javascript.

## Servidor de desarrollo

Ejecute `ng serve` para el servidor de desarrollo.

## Variables de desarrollo

Crear un archivo `.env` al copiar el repositorio y guiarse del archivo `.env.test` para crear las variables de entorno. 
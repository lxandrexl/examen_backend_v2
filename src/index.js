const express = require('express');
const cors = require('cors');
const logger = require('morgan');
const app = express();
const server = require('http').createServer(app);

require('dotenv').config();
require('./config/database');

//Dependencies
const Estudiante = require('./app/routes/estudiante.route');
const Pagos = require('./app/routes/pago.route');
const Reporte = require('./app/routes/reporte.route');

//Middlewares
app.use(cors());
app.use(express.json({ limit: '100mb' }));
app.use(express.urlencoded({ extended: true, limit: '100mb' }));
app.use(logger('dev'));

//Routes
app.use('/public', express.static('public'));
app.use('/estudiantes', Estudiante);
app.use('/pagos', Pagos);
app.use('/reportes', Reporte);

server.listen(process.env.PORT, () => {
    console.log('Servidor escuchando puerto', process.env.PORT);
});
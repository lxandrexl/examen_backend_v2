require('dotenv').config();

(async function () {
    await connectBD();
})();

function connectBD() {
    return new Promise(async (resolve, reject) => {
        global.pgp = require("pg-promise")({ noWarnings: false });

        const __conexion = `postgres://${process.env.USER_DB}:${process.env.PASSWORD_DB}@${process.env.HOST}:${process.env.PORT_DB}/${process.env.NAME_DB}`;
        global.dbp = global.pgp(__conexion);
        global.dbp.connect();
        return resolve(true);
    })
}
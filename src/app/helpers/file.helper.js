const moment = require('moment');
const fs = require('fs');
const dirname = require('path').dirname;

module.exports = {
    guardarImagen(base64) {
        const fileName = `${moment().format('YYYYMMDDHHmmss')}.png`;
        const path = `./public/imgs/${fileName}`;
        let data = base64.replace(/^data:image\/png;base64,/, "");
        fs.promises
            .mkdir(dirname(path), { recursive: true })
            .then(() => fs.promises.writeFile(path, data, 'base64'));

        return fileName;
    }
}
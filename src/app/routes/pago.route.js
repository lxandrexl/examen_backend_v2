const express = require('express');
const router = express.Router();
const PagoController = require('../controllers/pago.controller');

router.get('/movimientos', PagoController.ListarMovimientos);
router.post('/pago', PagoController.ActualizarPagoMovimiento);
router.post('/anulacion', PagoController.ActualizarAnulacionMovimiento);

module.exports = router;
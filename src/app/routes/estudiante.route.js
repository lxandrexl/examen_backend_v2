const express = require('express');
const router = express.Router();
const EstudianteController = require('../controllers/estudiante.controller');

router.get('/', EstudianteController.Listar);
router.post('/', EstudianteController.Crear);
router.post('/delete', EstudianteController.Eliminar);

router.get('/grados', EstudianteController.ListarGrados);

module.exports = router;
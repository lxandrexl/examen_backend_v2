const express = require('express');
const router = express.Router();
const ReporteController = require('../controllers/reporte.controller');

router.get('/', ReporteController.listarReportes);
router.post('/buscar-items', ReporteController.listarItemsReporte);

module.exports = router;
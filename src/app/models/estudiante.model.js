module.exports = {
    insertPersona(datos) {
        return new Promise((resolve, reject) => {
            let sql = `CALL insertEstudiante($1, $2, $3, $4, $5, $6)`;
            sql = global.pgp.as.format(sql,
                [datos.nom_persona, datos.ape_pate_pers, datos.ape_mate_pers, datos.fecha_naci, datos.foto_ruta, datos.id_grado]);
            global.dbp.result(sql).then(data => {
                return resolve(data);
            }).catch(err => {
                return reject(err);
            });
        });
    },

    deletePersona(idPersona) {
        return new Promise((resolve, reject) => {
            let sql = `DELETE FROM persona WHERE id_persona = $1`;
            sql = global.pgp.as.format(sql, [idPersona]);
            global.dbp.any(sql).then((data) => {
                return resolve(data);
            }).catch((err) => {
                return reject(err);
            });
        });
    },

    deletePersonaMovimientos(idPersona) {
        return new Promise((resolve, reject) => {
            let sql = `DELETE FROM movimiento WHERE id_persona = $1`;
            sql = global.pgp.as.format(sql, [idPersona]);
            global.dbp.any(sql).then((data) => {
                return resolve(data);
            }).catch((err) => {
                return reject(err);
            });
        });
    },

    getPersonaMovimientos(idPersona) {
        return new Promise((resolve, reject) => {
            let sql = `select * from movimiento WHERE id_persona=$1`;
            sql = global.pgp.as.format(sql, [idPersona]);
            global.dbp.any(sql).then((data) => {
                return resolve(data);
            }).catch((err) => {
                return reject(err);
            });
        });
    },

    getLastInsertPersona() {
        return new Promise((resolve, reject) => {
            let sql = `select * from persona order by id_persona desc limit 1`;
            sql = global.pgp.as.format(sql);
            global.dbp.any(sql).then((data) => {
                return resolve(data);
            }).catch((err) => {
                return reject(err);
            });
        });
    },

    getPersonas() {
        return new Promise((resolve, reject) => {
            let sql = `SELECT * FROM persona ORDER BY id_persona desc`;
            sql = global.pgp.as.format(sql);
            global.dbp.any(sql).then((data) => {
                return resolve(data);
            }).catch((err) => {
                return reject(err);
            });
        });
    },

    getPersona(idPersona) {
        return new Promise((resolve, reject) => {
            let sql = ` SELECT p.*, g.desc_grado, g.nivel 
                        FROM persona p INNER JOIN grado g 
                        ON p.id_grado = g.id_grado WHERE p.id_persona = $1`;
            sql = global.pgp.as.format(sql, [idPersona]);
            global.dbp.one(sql).then(data => {
                return resolve(data);
            }).catch(err => {
                return reject(err);
            });
        });
    },

    getGrados() {
        return new Promise((resolve, reject) => {
            let sql = `SELECT * FROM grado order by orden_grado ASC`;
            sql = global.pgp.as.format(sql);
            global.dbp.any(sql).then((data) => {
                return resolve(data);
            }).catch((err) => {
                return reject(err);
            });
        });
    },
};

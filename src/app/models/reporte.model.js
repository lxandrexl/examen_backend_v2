module.exports = {
    getReportes() {
        return new Promise((resolve, reject) => {
            let sql = `SELECT * FROM documento_new ORDER BY id_documento desc`;
            sql = global.pgp.as.format(sql);
            global.dbp.any(sql).then((data) => {
                return resolve(data);
            }).catch((err) => {
                return reject(err);
            });
        });
    },
    
    getItemsReporte(idDocumento) {
        return new Promise((resolve, reject) => {
            let sql = ` SELECT dc.desc_pension as "__pension", m.*  FROM movimiento m
                        INNER JOIN detalle_cronograma dc
                        ON m.id_detalle_cronograma = dc.id_detalle_cronograma
                        INNER JOIN documento_x_audi_movimiento dxa
                        ON m.id_movimiento = dxa.id_movimiento
                        INNER JOIN documento_new dn
                        ON dxa.id_documento = dn.id_documento
                        WHERE dn.id_documento = $1
                        ORDER BY m.id_movimiento ASC`;
            sql = global.pgp.as.format(sql, [idDocumento]);
            global.dbp.any(sql).then(data => {
                return resolve(data);
            }).catch(err => {
                return reject(err);
            });
        });
    }
}
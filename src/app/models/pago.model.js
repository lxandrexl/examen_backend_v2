module.exports = {
    getMovimientos() {
        return new Promise((resolve, reject) => {
            let sql = ` SELECT m.*, dc.desc_pension as "__pension", dc.fecha_venci as "__fecha_venci" 
                        FROM movimiento m
                        INNER JOIN detalle_cronograma dc
                        ON m.id_detalle_cronograma = dc.id_detalle_cronograma
                        ORDER BY m.id_movimiento ASC`;
                        
            sql = global.pgp.as.format(sql);
            global.dbp.any(sql).then((data) => {
                return resolve(data);
            }).catch((err) => {
                return reject(err);
            });
        });
    },

    insertBoletaPagos(datos) {
        return new Promise((resolve, reject) => {
            let sql = `CALL insertBoletaPagos($1, $2)`;
            sql = global.pgp.as.format(sql, [datos, 0]);
            global.dbp.result(sql).then(data => {
                return resolve(data);
            }).catch(err => {
                return reject(err);
            });
        });
    },

    insertItemBoletaPago(movimientId,boletaId, correlativo) {
        return new Promise((resolve, reject) => {
            let sql = `CALL insertItemBoletaPagos($1, $2, $3)`;
            sql = global.pgp.as.format(sql, [boletaId, movimientId, correlativo]);
            global.dbp.result(sql).then(data => {
                return resolve(data);
            }).catch(err => {
                return reject(err);
            });
        });
    },

    setAccionMovimiento(datos, accion) {
        return new Promise((resolve, reject) => {
            let sql = `CALL updateEstadoMovimiento($1, $2, $3)`;
            const params = [datos.id_movimiento, accion, datos.monto];
            sql = global.pgp.as.format(sql, params);
            global.dbp.any(sql).then(rs => {
                return resolve({ result: 'inserted', status: 'success', updated: true });
            }).catch(err => {
                return reject({ err, updated: false });
            });
        });
    },


};

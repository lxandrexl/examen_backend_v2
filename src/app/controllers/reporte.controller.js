const HttpStatus = require('http-status-codes');
const ReporteModel = require('../models/reporte.model');

module.exports = {
    async listarReportes(req, res) {
        let reportes = await ReporteModel.getReportes();

        return res
            .status(HttpStatus.OK)
            .json(reportes);
    },

    async listarItemsReporte(req, res) {
        const idReporte = req.body.id;
        
        let items = await ReporteModel.getItemsReporte(idReporte);

        return res
            .status(HttpStatus.OK)
            .json(items);
    }
}
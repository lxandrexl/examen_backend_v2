const HttpStatus = require('http-status-codes');
const EstudianteModel = require('../models/estudiante.model');
const FileHelper = require('../helpers/file.helper');

module.exports = {
    async Listar(req, res) {
        let estudiantes = await EstudianteModel.getPersonas();

        return res
            .status(HttpStatus.OK)
            .json(estudiantes);
    },

    async Crear(req, res) {
        req.body.foto_ruta = FileHelper.guardarImagen(req.body.foto_ruta);

        await EstudianteModel.insertPersona(req.body);

        let estudiante = await EstudianteModel.getLastInsertPersona();

        return res
            .status(HttpStatus.OK)
            .json(estudiante[0]);
    },

    async Eliminar(req, res) {
        let idPersona = req.body.id;
        let findMovimiento = false;

        let movimientos = await EstudianteModel.getPersonaMovimientos(idPersona);

        movimientos.map((mov) => {
            if (mov.estado != 'POR PAGAR') findMovimiento = true;
        });

        if (!findMovimiento) {
            let rs2 = await EstudianteModel.deletePersonaMovimientos(idPersona);
            let rs1 = await EstudianteModel.deletePersona(idPersona);

            return res
                .status(HttpStatus.StatusCodes.OK)
                .json({ 
                    message: 'Se elimino al estudiante correctamente.', 
                    type: 'success', 
                    personaAction: rs1, movimientoAction: rs2 
                });
        }

        return res
            .status(HttpStatus.StatusCodes.OK)
            .json({ 
                message: 'No se pudo eliminar al estudiante porque ha realizado pagos.', 
                type: 'failed',
                movimientos });
    },

    async ListarGrados(req, res) {
        let grados = await EstudianteModel.getGrados();

        return res
            .status(HttpStatus.OK)
            .json(grados)
    }
}
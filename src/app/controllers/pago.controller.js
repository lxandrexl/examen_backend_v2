const HttpStatus = require('http-status-codes');
const PagoModel = require('../models/pago.model');
const EstudianteModel = require('../models/estudiante.model');

module.exports = {
    async ListarMovimientos(req, res) {
        let movimientos = await PagoModel.getMovimientos();

        return res
            .status(HttpStatus.OK)
            .json(movimientos);
    },

    async ActualizarPagoMovimiento(req, res) {
        let movimientos = req.body;
        let result = [];
        let monto_total = 0;
        let count = 1;
        let est = await EstudianteModel.getPersona(movimientos[0].id_persona);

        movimientos.map((mov) => monto_total += Number(mov.monto));

        const document = {
            monto_total,
            nombres: `${est.nom_persona} ${est.ape_pate_pers} ${est.ape_mate_pers}`,
            grado: `${est.desc_grado} ${est.nivel}`
        }

        let documentResult = await PagoModel.insertBoletaPagos(document);
        let documentId = documentResult.rows[0].lastid;


        for (let mov of movimientos) {
            let rs = await PagoModel.setAccionMovimiento(mov, 'PAGADO');
            if (!!rs && rs.updated) {
                //Guardar boleta con items
                await PagoModel.insertItemBoletaPago(mov.id_movimiento, documentId, count);
            }
            result.push(rs)
            count++;
        }

        return res
            .status(HttpStatus.OK)
            .json(result);
    },

    async ActualizarAnulacionMovimiento(req, res) {
        let movimientos = req.body;
        let result = [];

        for (let mov of movimientos) {
            let rs = await PagoModel.setAccionMovimiento(mov, 'ANULADO');
            result.push(rs);
        }

        return res
            .status(HttpStatus.OK)
            .json(result);
    }
}